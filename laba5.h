#include <stdio.h>
#include <string.h>
#include "cbmp.h"
#include <time.h>

void SINGLE_COLOR_CHANNEL(BMP* img);
void processing(pixel **pixels, const int size);
unsigned int OPEN(BMP* img, const char* inputpath);
unsigned int CLOSE(BMP* img, const char* outputpath);
void processing_asm (pixel *pixels, const int size);
void SINGLE_COLOR_CHANNEL_ASM(BMP* img);
