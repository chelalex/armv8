	.arch armv8-a
	.data
number:
	.equ	const1, 0xff000000
	.equ	const2, 0x000000ff
	.equ	const3, 0x0000ff00
	.equ	const4, 0x00ff0000
	.text
	.align 2
	.global processing_asm
	.type   processing_asm, %function
processing_asm:
	stp	x29, x30, [sp, #-16]!
	mov     x20, #0//counter
//	adr	x21, number
//	ldr	w21, [x21]
	mov	w21, const1
	mov	w22, const2
	mov	w23, const3
	mov	w24, const4
	mov	x28, x0
0:
	cbz     x0, 1f
	//mov     x28, x0 //save prev address to an address to a struct
	//w1 is saving size of mass
	ldr     w2, [x0]
	//cbz     x2, 1f
	//ldr	w3, [x2]//chars: red, green, blue, alpha
	and     w3, w2, w22//red
//	and	w4, w2, w23//green
//	lsr	w4, w4, #8
//	and	w5, w2, w24//blue
//	lsr	w5, w5, #16
	and	w2, w2, w21
//	add	w3, w3, w4
//	add	w3, w3, w5
//	mov	w6, #3
//	udiv	w3, w3, w6
	lsl	w4, w3, #8
	add	w4, w4, w3
	lsl	w4, w4, #8
	add	w4, w4, w3
	add	w2, w2, w4
	str	w2, [x0], #4
	cmp     w20, w1//counter and size
	beq     1f
	add     w20, w20, #1
	blt     0b
1:
	ldp	x29, x30, [sp], #16
	mov	x0, x28
	ret
	.size   processing_asm, .-processing_asm
