#include "laba5.h"


void SINGLE_COLOR_CHANNEL(BMP* img){
    struct timespec t1, t2, t;
    int size = img->height * img->width;
    pixel *pixels = img->pixels;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
    processing(&pixels, size);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);
    t.tv_sec = t2.tv_sec - t1.tv_sec;
    if((t.tv_nsec = t2.tv_nsec - t1.tv_nsec) < 0){
    	t.tv_sec--;
    	t.tv_nsec += 1000000000;
    }
    printf("Single color channel on C: %ld.%09ld \n", t.tv_sec, t.tv_nsec);
    //return img;
}

void processing (pixel **pixels, const int size){
    pixel *ptr = *pixels;
    for(int i = 0; i < size; i++, ptr++){
        int grey = ptr->red;
	ptr->red = grey;
	ptr->blue = grey;
	ptr->green = grey;
    }
}

unsigned int OPEN(BMP* img, const char* inputpath){
    img = bopen(inputpath);
    if (img)
        return 1;
    else
        return 0;
}

unsigned int CLOSE(BMP* img, const char* outputpath){
    if (img){
        bwrite(img, outputpath);
        return 1;
    }
    return 0;
}

void SINGLE_COLOR_CHANNEL_ASM(BMP* img){
	struct timespec t1, t2, t;
	int size = img->height * img->width;
	pixel *pixels = img->pixels;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
	processing_asm(pixels, size);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);
	t.tv_sec = t2.tv_sec - t1.tv_sec;
	if((t.tv_nsec = t2.tv_nsec - t1.tv_nsec) < 0){
        	t.tv_sec--;
        	t.tv_nsec += 1000000000;
	}
	printf("Single color channel on ASM: %ld.%09ld \n", t.tv_sec, t.tv_nsec);
	//return img;
}
