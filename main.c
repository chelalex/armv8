#include "laba5.h"
#include <stdlib.h>
#include <string.h>
//"/Users/aleksandracelnokova/Desktop/Элтех/laba5.c/laba5.c/scale_1200.bmp"

char *getstr(char *a){
	a = (char*)malloc(1);
    *a = '\0';
    int length = 0, n;
    char buf[81];
    do{
        n = scanf("%80[^\n]", buf);
        if (n < 0){
            free(a);
            a = NULL;
            continue;
        }
        if (n > 0){
            length += strlen(buf);
            char *sym = &buf[0];
            for (;*sym; sym++) if(((*sym>=33)&&(*sym<=47))||((*sym>=58)&&(*sym<=126))) *sym = ' ';
            a = (char*)realloc(a, length +1);
            strcat(a, buf);
        }
    }while (n > 0);
    return a;
}


int main(int argc, const char * argv[]) {
//int main(const char* inputfile, const char* outputfile){
    char* inputfile = "unnamed.bmp";
//    printf("Enter name of input file\n");
//    getstr(inputfile);
    char* outputfile = "result2.bmp";
    //printf("Enter name of outputfile\n");
    //getstr(outputfile);
//    int len = strlen(inputfile);
//    outputfile = (char*)malloc(len + 3);
//    strncat(outputfile, inputfile, len - 4);
//    strncat(outputfile, "_1.bmp", 5);
    BMP* input_img = NULL;
    input_img = bopen(inputfile);
//    BMP* output1_img = NULL, output2_img = NULL;
    SINGLE_COLOR_CHANNEL_ASM(input_img);
    SINGLE_COLOR_CHANNEL(input_img);
    bwrite(input_img, outputfile);
    return 0;
}
